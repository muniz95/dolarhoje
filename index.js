const DOLARHOJE_URL = 'http://dolarhoje.com/';
const cheerio = require("cheerio");
const rp = require('request-promise');

const getPage = () => {
    return rp(DOLARHOJE_URL)
    .then(response => response)
    .catch(err => err.error.code);
}

const dolarHoje = () => {
    return getPage()
    .then(r => {
        const $ = cheerio.load(r);
        return $('#nacional').val();
    })
    .catch((err) => {
        console.log(err.error);
    });
}

module.exports = {
    dolarHoje,
    getPage
}