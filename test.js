'use strict';

const expect = require('chai').expect;
const getPage = require('./index').getPage;
const dolarHoje = require('./index').dolarHoje;
const cheerio = require("cheerio");

describe('#DolarHoje', () => {
    it('should return a valid HTML page', () => {
        return getPage().then(result => expect(result).to.not.equal('ENOTFOUND'));
    });
    it('should return a number as currency value', () => {
        return dolarHoje().then(result => {
            const valor = parseFloat(result.replace(',','.'));
            return expect(valor).to.be.a('number');
        })
    });
});