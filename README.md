# dolarhoje #

Módulo em Node.js que retorna o valor do dólar no dia corrente. Ele realiza um scrapping no site [Dolar Hoje](http://dolarhoje.com), trazendo a cotação da moeda na data atual.